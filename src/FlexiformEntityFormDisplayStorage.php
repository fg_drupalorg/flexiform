<?php

namespace Drupal\flexiform;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Provides storage for entity view display entities.
 */
class FlexiformEntityFormDisplayStorage extends ConfigEntityStorage {

  /**
   * The name of the original entity class.
   *
   * @var string
   */
  private $originalEntityClass;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeInterface $entity_type, ConfigFactoryInterface $config_factory, UuidInterface $uuid_service, LanguageManagerInterface $language_manager, ?MemoryCacheInterface $memory_cache = NULL) {
    parent::__construct($entity_type, $config_factory, $uuid_service, $language_manager, $memory_cache);
    $this->originalEntityClass = $entity_type->get('original_class');
  }

  /**
   * {@inheritdoc}
   */
  protected function mapFromStorageRecords(array $records) {
    $entities = [];

    foreach ($records as $record) {
      $entity = $this->getEntityStorageClass($record);
      $entities[$entity->id()] = $entity;
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  protected function doCreate(array $values) {
    // Set default language to current language if not provided.
    $values += [$this->langcodeKey => $this->languageManager->getCurrentLanguage()->getId()];
    $entity = $this->getEntityStorageClass($values);
    return $entity;
  }

  /**
   * Get the entity class to use for the values.
   *
   * @param array $values
   *   The values.
   *
   * @return \Drupal\Core\Entity\Display\EntityDisplayInterface
   */
  private function getEntityStorageClass(array $values) {
    // If not explicitly disable, see if we are using any enhancers.
    if (empty($values['third_party_settings']['flexiform_disable']['disabled']) && !empty($values['third_party_settings']['flexiform']['enhancer'])) {
      foreach ($values['third_party_settings']['flexiform']['enhancer'] as $enhancer) {
        if (count($enhancer) > 1) {
          // Use the enhanced class.
          return new $this->entityClass($values, $this->entityTypeId);
        }
      }
    }

    // Otherwise don't!
    return new $this->originalEntityClass($values, $this->entityTypeId);
  }

}
